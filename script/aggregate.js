const path = require('path')
const fs = require('fs')
const { create } = require('xmlbuilder2')

const {
    DOC_FOLDER,
    SECTION_FOLDER,
    COURSE_FILE_NAME,
    getRootPath,
    getOutPath,
    getLanguages
} = require('./build')

const AGGREGATE_TIME = 'Aggregate'
console.time(AGGREGATE_TIME)

getLanguages().forEach(language => {
    const distLanguageFolder = path.join(getOutPath(), language.code)
    if (!fs.existsSync(distLanguageFolder)) {
        fs.mkdirSync(distLanguageFolder)
    }

    const languageFolder = path.join(getRootPath(), DOC_FOLDER, language.code)
    generateCourseXML(languageFolder)

    console.log('XML generated from section:', languageFolder)
})

console.timeEnd(AGGREGATE_TIME)

function generateCourseXML (languageFolder) {
    let course

    const sectionFolder = path.join(languageFolder, SECTION_FOLDER)
    const sectionFiles = fs.readdirSync(sectionFolder)
    sectionFiles.forEach((fileName, index) => {
        const sectionFile = path.join(sectionFolder, fileName)

        const xmlString = fs.readFileSync(sectionFile, { encoding: 'utf8', flag: 'r' })

        // First file is backward compatible (alphabetic order)
        // For every others file we use only the collection of section element
        if (index === 0) {
            course = create(xmlString)
        } else {
            const firstFileBodyElement = course.root().find(xmlBuilder => xmlBuilder.node.nodeName === 'body')

            // Parse the next file
            const sectionFile = create(xmlString)
            // Filter section elements
            const sections = sectionFile.filter(xmlBuilder => {
                return xmlBuilder.node.nodeName === 'section'
            }, false, true)
            // Append each section in the first file
            sections.forEach(section => firstFileBodyElement.import(section))
        }
    })

    fs.writeFileSync(
        path.join(getOutPath(), path.basename(languageFolder), COURSE_FILE_NAME + '.xml'),
        course.end({ prettyPrint: true })
    )
}
