const fs = require('fs')
const path = require('path')
const archiver = require('archiver')

const {
    DIST_FOLDER,
    OUT_FOLDER,
    DISTRIBUTION_ARCHIVE_FILE_NAME,
    getRootPath,
    formatBytes
} = require('./build')

const DISTRIBUTE_TIME = 'Distribute'

const archiveFileName = path.join(getRootPath(), DIST_FOLDER, DISTRIBUTION_ARCHIVE_FILE_NAME)
const outputStream = fs.createWriteStream(archiveFileName)
const archive = archiver('zip')

archive.on('error', function (error) {
    throw error
})

outputStream.on('close', function () {
    console.info('Archive created:', archiveFileName, formatBytes(archive.pointer()))
    console.timeEnd(DISTRIBUTE_TIME)
})

archive.pipe(outputStream)
archive.directory(path.join(getRootPath(), DIST_FOLDER, OUT_FOLDER), false)
// archive.append(generateVersion(), { name: packageJson.name + '/course-version.xml' })

console.time(DISTRIBUTE_TIME)
archive.finalize()

/* function generateVersion () {
  return builder.create('version', { encoding: 'utf-8' })
    .ele(
      'project',
      { name: packageJson.name, version: packageJson.version, date: new Date() },
      packageJson.description
    )
    .end({ pretty: true })
} */
