
# moodle-player

La version actuelle est seulement la structure complète d'un cours distribué via moodle.

Note: Le travail sur ce projet se limitera à la maintenance, les nouvelles idées seront développées dans [teaching-assitant](https://bitbucket.org/martinvachon/teaching-assistant/src/master/)

## Installation

Après avoir cloné le repo, copier et renommer le répertoire et la propriété **name** du fichier package.json pour créer un nouveau cours.

## Usage

Installation des dépendances nécessaires pour modifier le cours.
```bash
npm install
```

Voir le cours dans le navigateur.
```bash
npm start
```

Créer l'archive pour le téléchargement dans moodle
```bash
npm run dist
```

