const path = require('path')
const fs = require('fs')
const ISO6391 = require('iso-639-1')

const DOC_FOLDER = 'doc'
const DIST_FOLDER = 'dist'
const LIB_FOLDER = 'lib'
const OUT_FOLDER = 'out'
const ASSET_FOLDER = 'asset'
const SECTION_FOLDER = 'section'
const COURSE_FILE_NAME = 'index'

const DISTRIBUTION_ARCHIVE_FILE_NAME = 'html.zip'

/**
 * Helper function to return the project root folder
 * @returns Project root folder path
 */
function getRootPath () {
    return path.join(__dirname, '..')
}

function getOutPath () {
    return path.join(getRootPath(), DIST_FOLDER, OUT_FOLDER)
}

/**
 * Helper function for conversion of bytes to a readable value.
 * Reference: https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
 * @param {*} bytes
 * @param {*} decimals
 * @returns Formatted string
 */
function formatBytes (bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes'

    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
}

function getLanguages () {
    const docFolders = fs.readdirSync(path.join(getRootPath(), DOC_FOLDER))
    const languages = docFolders.filter(folderName => {
        const languageFolder = path.join(getRootPath(), DOC_FOLDER, folderName)
        const stats = fs.statSync(languageFolder)

        return folderName !== ASSET_FOLDER && stats.isDirectory() && ISO6391.validate(folderName)
    })

    return languages.map(language => {
        return {
            code: language,
            nativeName: ISO6391.getNativeName(language)
        }
    })
}

module.exports = {
    DOC_FOLDER,
    DIST_FOLDER,
    LIB_FOLDER,
    OUT_FOLDER,
    ASSET_FOLDER,
    SECTION_FOLDER,
    COURSE_FILE_NAME,
    DISTRIBUTION_ARCHIVE_FILE_NAME,
    getRootPath,
    getOutPath,
    formatBytes,
    getLanguages
}
