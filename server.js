'use strict'

const express = require('express')
const open = require('open')

const PORT = 80

const app = express()

app.use('/lib', express.static('lib'))

app.use(express.static('doc/fr/section'))
app.use('/asset', express.static('doc/fr/asset'))

app.use(express.static('doc/en/section'))
app.use('/asset', express.static('doc/en/asset'))

app.listen(PORT, () => open('http://localhost:' + PORT + '/moodle-player.xml'))
