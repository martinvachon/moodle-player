const childProcess = require('child_process')
const path = require('path')

const {
    LIB_FOLDER,
    COURSE_FILE_NAME,
    getRootPath,
    getOutPath,
    getLanguages
} = require('./build')

const xslt3Command = path.join(getRootPath(), 'node_modules/.bin/xslt3')
const xslFile = path.join(getRootPath(), LIB_FOLDER, 'moodle-player/moodle-player.xslt')

getLanguages().forEach(language => {
    const xsltTimer = 'XSLT ' + language.code
    console.time(xsltTimer)

    generateCourseHTML(language.code)

    console.timeEnd(xsltTimer)
    console.log('HTML generated from:', language.code)
})

function generateCourseHTML (language) {
    const xmlFile = path.join(getOutPath(), language, COURSE_FILE_NAME + '.xml')
    const resultFile = path.join(getOutPath(), language, COURSE_FILE_NAME + '.html')

    const xslt3Params = ['-s:' + xmlFile, '-xsl:' + xslFile, '-o:' + resultFile]

    console.info('xslt3 params:', xslt3Params)

    const result = childProcess.spawnSync(xslt3Command, xslt3Params, { shell: true })
    if (result.status !== 0) {
        process.stderr.write(result.stderr)
        process.exit(result.status)
    } else {
        process.stdout.write(result.stdout)
        process.stderr.write(result.stderr)
    }
}
