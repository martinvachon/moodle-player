<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="about:legacy-compat" />

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="index">
        <xsl:element name="html">
            <xsl:attribute name="lang">en</xsl:attribute>
            <xsl:element name="head">
                <xsl:element name="title">Index</xsl:element>
            </xsl:element>
            <xsl:element name="body">
                <xsl:apply-templates />
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template match="languages">
        <xsl:element name="ul">
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <xsl:template match="language">
        <xsl:element name="li">
            <xsl:element name="a">
                <xsl:attribute name="href">
                    <xsl:value-of select="concat(@code, '/index.html')" />
                </xsl:attribute>
                <xsl:value-of select="@nativeName" />
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>