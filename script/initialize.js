const path = require('path')
const copyDir = require('copy-dir')
const fs = require('fs')
const { create } = require('xmlbuilder2')
const childProcess = require('child_process')

const {
    DOC_FOLDER,
    LIB_FOLDER,
    getRootPath,
    getOutPath,
    getLanguages
} = require('./build')

const INITIALIZE_TIME = 'Initialize'
console.time(INITIALIZE_TIME)
copyDocFolder()
copyLibfolder()
generateIndexPageXML()
transformIndexPageHTML()
console.timeEnd(INITIALIZE_TIME)

function copyDocFolder () {
    const outFolder = getOutPath()
    if (!fs.existsSync(outFolder)) {
        fs.mkdirSync(outFolder)
    }
    copyDir.sync(path.join(getRootPath(), DOC_FOLDER), outFolder, {
        filter: function (stat, filepath, filename) {
            return stat === 'directory' && filename === 'section'
        }
    })
    console.log('Doc asset copied')
}

function copyLibfolder () {
    const libPath = path.join(getOutPath(), LIB_FOLDER)
    if (!fs.existsSync(libPath)) {
        fs.mkdirSync(libPath)
    }
    copyDir.sync(path.join(getRootPath(), LIB_FOLDER), libPath)
    console.log('Doc lib copied')
}

function generateIndexPageXML () {
    const indexElement = create({ encoding: 'utf-8' }).ele('index')
    const languageElements = indexElement.ele('languages')

    getLanguages().forEach(language => {
        languageElements.ele('language', { code: language.code, nativeName: language.nativeName })
    })

    fs.writeFileSync(
        path.join(getOutPath(), 'index.xml'),
        indexElement.end({ prettyPrint: true })
    )
}

function transformIndexPageHTML () {
    const xslt3Command = path.join(getRootPath(), 'node_modules/.bin/xslt3')
    const xslFile = path.join(getRootPath(), LIB_FOLDER, 'index-page/index-page.xslt')

    const xmlFile = path.join(getOutPath(), 'index.xml')
    const resultFile = path.join(getOutPath(), 'index.html')

    const xslt3Params = ['-s:' + xmlFile, '-xsl:' + xslFile, '-o:' + resultFile]

    console.info('xslt3 params:', xslt3Params)

    const result = childProcess.spawnSync(xslt3Command, xslt3Params, { shell: true })
    if (result.status !== 0) {
        process.stderr.write(result.stderr)
        process.exit(result.status)
    } else {
        process.stdout.write(result.stdout)
        process.stderr.write(result.stderr)
    }
}
